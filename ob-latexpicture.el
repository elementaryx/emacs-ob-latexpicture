(require 'ox-latex)
(require 'ob-latex)

(add-to-list 'org-src-lang-modes '("latexpicture" . latex))

;; (org-babel-lob-ingest "./ob-latexpicture-lob.org")

(defconst org-babel-header-args:latexpicture
  (add-to-list 'org-babel-header-args:latex
	       '(usemacros      . ((nil t))))
  "latexpicture-specific header arguments.")

(defun org-src-sha ()
  (let ((elem (org-element-at-point)))
    (concat (sha1 (org-element-property :value elem)) ".svg")))

;; (defvar/setq org-babel-default-header-args:latexpicture
(defvar org-babel-default-header-args:latexpicture
  '(;;(:results . (lambda() (if (eq org-export-current-backend 'html) "output graphics file" "latex")))
    (:results . "file drawer")
    ;; (:file . (lambda () (if (eq org-export-current-backend 'html) (org-src-sha) nil)))
    (:file . (lambda () (org-src-sha)))
    (:exports . "results")
    (:usemacros . nil)))

(defcustom org-babel-latex-latexpicture-packages
  '("[usenames]{color}" "{tikz}" "{color}" "{listings}" "{braket}" "{amsmath}" "{amsfonts}" "{amsopn}" "[algo2e,ruled,vlined]{algorithm2e}")
  "Packages to use for latexpicture export."
  :group 'org-babel
  :type '(repeat (string)))

(defcustom org-babel-latexpicture-preamble
  (lambda (_)
    (concat
     "\\documentclass[preview]{standalone}\n"
     (mapconcat (lambda (pkg)
		  (concat "\\usepackage" pkg))
		org-babel-latex-latexpicture-packages
		"\n")))
  "Closure which evaluates at runtime to the LaTeX picture preamble.

  It takes 1 argument which is the parameters of the source block."
  :group 'org-babel
  :type 'function)

(defcustom org-babel-latexpicture-begin-env
  (lambda (params)
    (message "org-babel-latexmacro-get-current-org-src-source-file-name: %s" (org-babel-latexmacro-get-current-org-src-source-file-name))
    (let ((usemacros (cdr (assq :usemacros params)))
	  (latexmacro-file-name (concat
				 (file-name-sans-extension (org-babel-latexmacro-get-current-org-src-source-file-name))
				 ".latexmacro")))
      (message "usemacros is now %s and latexmacro-filename is %s" usemacros latexmacro-file-name)
      (concat
       (if usemacros (concat "\n\\include{"
			     latexmacro-file-name
			     "}\n")
	 "")
       "\\begin{document}\n")))
  "Functionality that evaluates to the begin part of the document environment.

It takes 1 argument which is the parameters of the source block.
This-Start allows adding additional code that will be ignored when
exporting the literal LaTeX source."
  :group 'org-babel
  :type 'function)

(setq org-babel-latex-preamble org-babel-latexpicture-preamble)

(setq org-babel-latex-begin-env org-babel-latexpicture-begin-env)

(defun org-babel-execute:latexpicture (body params)
  "Execute LaTeXpicture BODY according to PARAMS.
This function is called by `org-babel-execute-src-block'."
  (org-babel-execute:latex body params))

(provide 'ob-latexpicture)
